package cam.nwe23.chempound;

	import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import nu.xom.ParsingException;

import org.apache.abdera.Abdera;
import org.apache.abdera.model.Document;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.swordapp.client.Deposit;
import org.swordapp.client.DepositReceipt;
import org.swordapp.client.EntryPart;
import org.swordapp.client.ProtocolViolationException;
import org.swordapp.client.SWORDClient;
import org.swordapp.client.SWORDClientException;
import org.swordapp.client.SWORDError;
import org.swordapp.client.UriRegistry;
import org.xmlcml.cif.CIF;
import org.xmlcml.cif.CIFException;

import uk.ac.cam.ch.wwmm.chempound.content.ContentLoadRequest;
import uk.ac.cam.ch.wwmm.chempound.crystal.importer.CrystalStructureImporter;
import uk.ac.cam.ch.wwmm.chempound.crystal.importer.utils.CifIO;
import uk.ac.cam.ch.wwmm.chempound.rdf.DCTerms;
import uk.ac.cam.ch.wwmm.chempound.storage.DepositResource;

import com.hp.hpl.jena.rdf.model.Statement;
import com.hp.hpl.jena.xmloutput.RDFXMLWriterI;
import com.hp.hpl.jena.xmloutput.impl.Abbreviated;

	/**
	 * @author nwe23
	 */
	public class Depositor {
		
		String swordEndpoint;
		File cifFile;
		String doi;

		public Depositor(){
			this.swordEndpoint="http://hendra.ch.cam.ac.uk:8080/sword/collection/";
			init();
		}
		
		public Depositor(String swordEndpoint){
			this.swordEndpoint=swordEndpoint;
			init();
		}
		
		private void init(){
			
		}
		
		public void deposit(String doi, File cifFile){
			this.doi=doi;
			this.cifFile=cifFile;
			
		}
		
		public void depoist(String doi){
			this.doi=doi;
			fetchCifFromDoi();
		}
		
	    private void fetchCifFromDoi() {
			
		}

		public static void main(String[] args) throws SWORDClientException, SWORDError, ProtocolViolationException, IOException, CIFException, ParsingException {
	        
	    	Depositor depositor = new Depositor();
	    	File file = depositor.cifFile;
	        if (!file.isFile()) {
	            System.err.println("File not found: "+file);
	            System.exit(1);
	        }

	        Deposit deposit = prepareDeposit(file);

	        SWORDClient client = new SWORDClient();
	        DepositReceipt receipt = client.deposit(depositor.swordEndpoint, deposit);

	        if (receipt == null) {
	            System.err.println();
	            System.err.println("**                             **");
	            System.err.println("** DEPOSIT FAILED : NO RECEIPT **");
	            System.err.println("**                             **");
	            System.err.println();
	            System.exit(1);
	        }

	        System.err.println("** Deposit complete");
	        System.err.println("**   status: "+receipt.getStatusCode());
	    }

	    private static Deposit prepareDeposit(File file) throws CIFException, IOException, ParsingException {
	        CIF cif = CifIO.readCif(file, "UTF-8");

	        String id = getId(file);
	        CrystalStructureImporter importer = new CrystalStructureImporter();
	        ContentLoadRequest request = importer.generateDepositRequest(cif, id);

	        return createDeposit(request);
	    }

	    private static String getId(File file) {
	        String filename = file.getName();
	        String ext = FilenameUtils.getExtension(filename).toLowerCase();
	        if ("cif".equals(ext) || "txt".equals(ext)) {
	            int l = filename.length();
	            filename.substring(0, l-4);
	        }
	        return filename;
	    }


	    private static Deposit createDeposit(ContentLoadRequest request) throws IOException {
	        Deposit deposit = new Deposit();

	        EntryPart entryPart = new EntryPart();
	        String title = "Crystal Structure";

	        if (!request.getMetadataModel().isEmpty()) {
	            attachMetaData(request, entryPart);
	            Statement s = request.getMetadataModel().getResource("").getRequiredProperty(DCTerms.title);
	            if (s != null) {
	                title = s.getString();
	            }
	        }
	        entryPart.getEntry().setTitle(title);
	        deposit.setEntryPart(entryPart);

	        byte[] zip = createZipFile(request);

	        deposit.setFile(new ByteArrayInputStream(zip));
	        deposit.setMimeType("application/zip");
	        deposit.setFilename("deposit.zip");
	        deposit.setMd5(DigestUtils.md5Hex(zip));
	        deposit.setPackaging(UriRegistry.PACKAGE_SIMPLE_ZIP);

	        return deposit;
	    }

	    private static byte[] createZipFile(ContentLoadRequest request) throws IOException {
	        ByteArrayOutputStream buffer = new ByteArrayOutputStream();
	        ZipOutputStream out = new ZipOutputStream(buffer);
	        for (DepositResource resource : request.getAggregatedResources()) {
	            ZipEntry zipEntry = new ZipEntry(resource.getUri().toString());
	            out.putNextEntry(zipEntry);
	            InputStream in = resource.openInputStream();
	            IOUtils.copy(in, out);
	            IOUtils.closeQuietly(in);
	        }
	        out.close();

	        return buffer.toByteArray();
	    }

	    private static void attachMetaData(ContentLoadRequest request, EntryPart entryPart) {
	        ByteArrayOutputStream buffer = new ByteArrayOutputStream();

	        RDFXMLWriterI w = new Abbreviated();
	        w.setProperty("allowBadURIs", true);
	        w.write(request.getMetadataModel(), buffer, null);

	        Abdera abdera = new Abdera();
	        Document doc = abdera.getParser().parse(new ByteArrayInputStream(buffer.toByteArray()));
	        entryPart.getEntry().addExtension(doc.getRoot());
	    }

	}
