package cam.nwe23.config;

import java.util.Properties;

public class Configuration {
	final public String userAgent;
	final public String wordpressXmlRpcURL;
	final public String wordpressUsername;
	final public String wordpressPassword;
	final public String mongoURL;
	final public String proxyURL;
	final public Integer proxyPort;
	final public String dataBaseName;
	final public String httpCacheName;
	final public String filterType;
	final public String filterProperty;
	final public String filterValue;
	final public String filterComparator;
	final public Boolean postImmediately;

	
	public Configuration(Properties props){
		this.wordpressXmlRpcURL=props.getProperty("wordpressXmlRpcURL");
		this.wordpressUsername=props.getProperty("wordpressUsername");
		this.wordpressPassword=props.getProperty("wordpressPassword");
		this.mongoURL=props.getProperty("mongoURL");
		this.userAgent=props.getProperty("userAgent");
		this.proxyURL=props.getProperty("proxyURL");
		Integer port=null;
		try{
			port=Integer.parseInt(props.getProperty("proxyPort"));
		}catch (NumberFormatException e) {
			port=80;
		}
		this.proxyPort=port;
		this.dataBaseName=props.getProperty("dataBaseName");
		this.httpCacheName=props.getProperty("httpCacheName");
		this.filterType=props.getProperty("filterType");
		this.filterProperty=props.getProperty("filterProperty");
		this.filterValue=props.getProperty("filterValue");
		this.filterComparator=props.getProperty("filterComparator");
		this.postImmediately=Boolean.parseBoolean(props.getProperty("postImmediately"));
	}
}
