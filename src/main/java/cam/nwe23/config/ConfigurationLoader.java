package cam.nwe23.config;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Properties;

import org.apache.commons.io.IOUtils;

public class ConfigurationLoader {
	protected static Configuration staticConf;
	Properties props = new Properties();
	String configLocation="/config.properties";

	public ConfigurationLoader(){
		loadFromClassLoader();
	}
	
	public Configuration getConfiguration(){
		return new Configuration(props);
	}
	
	public ConfigurationLoader(String string) {
		this.configLocation=string;
		this.loadFromFile();
	}
	


	public static Configuration getStaticConfiguartion(){
		if(staticConf==null){
			ConfigurationLoader loader = new ConfigurationLoader();
			staticConf=new Configuration(loader.props);
		}
		return staticConf;
	}

	private void loadFromFile() {
		FileReader fileR=null;
		try {
			fileR= new FileReader(new File(configLocation));
			this.props.load(fileR);
		} catch (Exception e) {
			IOUtils.closeQuietly(fileR);
			System.err.println("Error loading config file, using defaults..");
			props=this.defaultConfig();
		}
		finally{
			IOUtils.closeQuietly(fileR);
		}
	}
	
	private void loadFromClassLoader(){
		try {
			this.props.load((this.getClass().getResourceAsStream(configLocation)));
		} catch (Exception e) {
			System.err.println("Error loading config file, using defaults..");
			props=this.defaultConfig();
		}
	}
	
	public void writeCurrentProps(String configName) {
		FileWriter writer=null;
		try {
			writer = new FileWriter(new File(configName));
			props.store(writer,"Writing Configuration");
		} catch (IOException e) {
			IOUtils.closeQuietly(writer);
		}
		finally{
			IOUtils.closeQuietly(writer);
		}
		
	}

	public Properties getProperties(){
		return this.props;
	}

	private Properties defaultConfig() {
		Properties defaultProps = new Properties();
		defaultProps.put("wordpressXmlRpcURL","http://localhost:8000/wordpress/xmlrpc.php");
		defaultProps.put("wordpressUsername","Chris Talbot");
		defaultProps.put("wordpressPassword","CrystalEye");
		defaultProps.put("mongoURL","wwmm-new.ch.private.cam.ac.uk");
		return defaultProps;
	}
}
