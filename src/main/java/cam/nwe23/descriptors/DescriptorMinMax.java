package cam.nwe23.descriptors;

import java.util.LinkedHashMap;
import java.util.Map;


public class DescriptorMinMax {

	Map<DescriptorTypes, MinMaxValue> min;
	Map<DescriptorTypes, MinMaxValue> max;

	public DescriptorMinMax() {
		init();
	}

	protected void init() {
		min = new LinkedHashMap<DescriptorTypes, MinMaxValue>(
				DescriptorTypes.values().length);
		max = new LinkedHashMap<DescriptorTypes, MinMaxValue>(
				DescriptorTypes.values().length);
		setMapTo(min, Double.MAX_VALUE);
		setMapTo(max, Double.MIN_VALUE);
	}

	protected void setMapTo(Map<DescriptorTypes, MinMaxValue> map,
			double valueToSet) {
		for (DescriptorTypes type : DescriptorTypes.values()) {
			MinMaxValue value = new MinMaxValue(valueToSet, null);
			map.put(type, value);
		}

	}
	
	public boolean addValue(DescriptorValues desc){
		boolean changed=false;
		if(desc==null){
			throw new IllegalArgumentException("Values must not be null");
		}
		for(DescriptorTypes type:DescriptorTypes.values()){
			double value = desc.getMap().get(type);
			if(value==Double.MAX_VALUE){
				continue;
			}
			if (value > max.get(type).getNumericValue()){
				this.max.put(type, new MinMaxValue(value, desc));
				changed=true;
			}
			if(value < min.get(type).getNumericValue()){
				this.min.put(type, new MinMaxValue(value, desc));
				changed=true;
			}
		}
		return changed;
	}
	
	public double getMinNumericValue(DescriptorTypes type){
		MinMaxValue old=this.min.get(type);
		return old.getNumericValue();
	}
	public DescriptorValues getMinValue(DescriptorTypes type){
		MinMaxValue old=this.min.get(type);
		return old.getDescriptorValue();
	}
	
	public double getMaxNumericValue(DescriptorTypes type){
		MinMaxValue old=this.max.get(type);
		return old.getNumericValue();
	}
	public DescriptorValues getMaxValue(DescriptorTypes type){
		MinMaxValue old=this.max.get(type);
		return old.getDescriptorValue();
	}
}
