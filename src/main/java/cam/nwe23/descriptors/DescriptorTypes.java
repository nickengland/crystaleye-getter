package cam.nwe23.descriptors;

import java.util.ArrayList;
import java.util.List;

import org.xmlcml.cml.base.CMLElement;
import org.xmlcml.cml.base.CMLElement.CoordinateType;
import org.xmlcml.cml.element.CMLAngle;
import org.xmlcml.cml.element.CMLAtom;
import org.xmlcml.cml.element.CMLAtomArray;
import org.xmlcml.cml.element.CMLBond;
import org.xmlcml.cml.element.CMLFormula;
import org.xmlcml.cml.element.CMLMolecule;

public enum DescriptorTypes{
	NUMBEROFATOMS("number of atoms") {
		@Override
		public double calcDescriptor(CMLMolecule mol,CMLElement root) {
			if (mol == null) {
				throw new IllegalArgumentException("Molecule must not be null");
			}
			CMLAtomArray array = mol.getAtomArray();
			if (array == null) {
				return Double.NaN;
			}
			return array.size();
		}
	},
	MOLECULARWEIGHT("molecular weight") {
		@Override
		public double calcDescriptor(CMLMolecule mol,CMLElement root) {
			if (mol == null) {
				throw new IllegalArgumentException("Molecule must not be null");
			}
			CMLFormula form = CMLFormula.createFormula(mol);
			return form.getCalculatedMolecularMass();
		}
	},
	MAXBONDLENGTH("maximum bond length") {
		@Override
		public double calcDescriptor(CMLMolecule mol,CMLElement root) {
			double maxLength = Double.MIN_VALUE;
			for (CMLBond bond : mol.getBonds()) {
				double length = bond.getBondLength(CoordinateType.CARTESIAN);
				if (maxLength < length) {
					maxLength = length;
				}
			}
			return maxLength;
		}
	},
	MINBONDLENGTH("minimum bond length") {
		@Override
		public double calcDescriptor(CMLMolecule mol,CMLElement root) {
			double minLength = Double.MAX_VALUE;
			for (CMLBond bond : mol.getBonds()) {
				double length = bond.getBondLength(CoordinateType.CARTESIAN);
				if (minLength > length) {
					minLength = length;
				}
			}
			return minLength;
		}
	},
	MAXBONDANGLE("maximum bond angle") {
		@Override
		public double calcDescriptor(CMLMolecule mol,CMLElement root) {
			if (mol == null) {
				throw new IllegalArgumentException("Molecule must not be null");
			}
			double maxAngle = Double.MIN_VALUE;
			for (CMLAtom atom : mol.getAtoms()) {
				List<CMLAtom> neigbours = atom.getLigandAtoms();
				if (neigbours.size() < 2) {
					continue;
				}
				for (int x = 0; x < neigbours.size() - 1; x++) {
					for (int y = x + 1; y < neigbours.size(); y++) {
						List<CMLAtom> list = new ArrayList<CMLAtom>(3);
						list.add(neigbours.get(x));
						list.add(atom);
						list.add(neigbours.get(y));
						CMLAngle angle = new CMLAngle();
						double value = angle.getCalculatedAngle(list);
						if (value > maxAngle) {
							maxAngle = value;
						}
					}
				}
			}
			return maxAngle;
		}
	},
	MINBONDANGLE("minimum bond angle") {
		@Override
		public double calcDescriptor(CMLMolecule mol,CMLElement root) {
			if (mol == null) {
				throw new IllegalArgumentException("Molecule must not be null");
			}
			double minAngle = Double.MAX_VALUE;
			for (CMLAtom atom : mol.getAtoms()) {
				List<CMLAtom> neigbours = atom.getLigandAtoms();
				if (neigbours.size() < 2) {
					continue;
				}
				for (int x = 0; x < neigbours.size() - 1; x++) {
					for (int y = x + 1; y < neigbours.size(); y++) {
						List<CMLAtom> list = new ArrayList<CMLAtom>(3);
						list.add(neigbours.get(x));
						list.add(atom);
						list.add(neigbours.get(y));
						CMLAngle angle = new CMLAngle();
						double value = angle.getCalculatedAngle(list);
						if (value < minAngle) {
							minAngle = value;
						}
					}
				}
			}
			return minAngle;
		}
	};

	DescriptorTypes(String reason){
		this.reason=reason;
	}
	
	private String reason;
	
	public String toString(){
		return reason;
	}
	public abstract double calcDescriptor(CMLMolecule mol, CMLElement root);

}
