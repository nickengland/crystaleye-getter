package cam.nwe23.descriptors;

import java.util.HashMap;
import java.util.Map;

import nu.xom.Nodes;

import org.xmlcml.cml.base.CMLConstants;
import org.xmlcml.cml.base.CMLElement;
import org.xmlcml.cml.element.CMLMolecule;

public class DescriptorValues{
	Map<DescriptorTypes, Double> valueMap;
	CMLMolecule mol;
	CMLElement root;

	@Deprecated
	public DescriptorValues(CMLMolecule mol) {
		this.mol = mol;
		this.root=null;
		this.valueMap=new HashMap<DescriptorTypes, Double>(DescriptorTypes.values().length);
		this.calc();
	}
	
	public DescriptorValues(CMLElement root) {
		this.root=root;
		Nodes nodes = root.query("//cml:molecule", CMLConstants.CML_XPATH);
		if (nodes.size() >= 1) {
			mol = (CMLMolecule) nodes.get(nodes.size() - 1);
		}
		this.valueMap=new HashMap<DescriptorTypes, Double>(DescriptorTypes.values().length);
		this.calc();
	}
	protected void calc() {
		for (DescriptorTypes type : DescriptorTypes.values()) {
			Double d = type.calcDescriptor(mol,root);
			valueMap.put(type, d);
		}
	}
	
	public Map<DescriptorTypes, Double> getMap(){
		return this.valueMap;
	}
	public CMLMolecule getMol(){
		return this.mol;
	}
	public CMLElement getRootElement(){
		return this.root;
	}
	
	public String toString(){
		return mol.getId()+" "+valueMap.toString();
	}
	public Map<DescriptorTypes, Double> getValueMap(){
		return this.valueMap;
	}
}
