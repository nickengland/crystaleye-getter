package cam.nwe23.descriptors;

import java.util.List;

import org.xmlcml.cml.base.CMLElement;

public class MinMaxFilter {

	DescriptorMinMax minMax;
	
	public MinMaxFilter(){
		init();
	}

	private void init(){
		minMax=new DescriptorMinMax();
	}
	
	public void addAllCmlElement(List<CMLElement> list){
		for(CMLElement root:list){
			this.addCMLElement(root);
		}
	}

	public DescriptorValues addCMLElement(CMLElement root){
		DescriptorValues values = new DescriptorValues(root);
		this.minMax.addValue(values);
		return values;
	}

	public double getMaxNumericValue(DescriptorTypes type) {
		return minMax.getMaxNumericValue(type);
	}

	public DescriptorValues getMaxValue(DescriptorTypes type) {
		return minMax.getMaxValue(type);
	}
	
	public double getMinNumericValue(DescriptorTypes type) {
		return minMax.getMinNumericValue(type);
	}
	
	public DescriptorValues getMinValue(DescriptorTypes type) {
		return minMax.getMinValue(type);
	}
	

	
	
}
