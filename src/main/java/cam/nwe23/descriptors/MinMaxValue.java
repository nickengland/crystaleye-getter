package cam.nwe23.descriptors;


class MinMaxValue {
	Double numericValue;
	DescriptorValues descriptorValues;

	public Double getNumericValue() {
		return numericValue;
	}

	public DescriptorValues getDescriptorValue() {
		return descriptorValues;
	}

	public MinMaxValue(double value, DescriptorValues desc) {
		this.numericValue = value;
		this.descriptorValues = desc;
	}

}