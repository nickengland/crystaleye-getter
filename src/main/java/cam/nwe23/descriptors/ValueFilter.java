package cam.nwe23.descriptors;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import nu.xom.Element;
import nu.xom.Elements;
import nu.xom.Nodes;
import nu.xom.XPathContext;

import org.xmlcml.cml.base.CMLConstants;
import org.xmlcml.cml.base.CMLElement;
import org.xmlcml.cml.converters.cif.dict.CifDictionaryBuilder;
import org.xmlcml.cml.element.CMLScalar;

import cam.nwe23.config.Configuration;

public class ValueFilter {
	String property;
	String value;
	String comparator;
	String xpath;
	XPathContext context;
	Class<?> type;
	Map<CMLElement, String> outputMap;
	public final String IUCRdictURL = "http://xml-cml.org/dictionary/cif/#";

	public String getReasonString(CMLElement root) {
		StringBuilder builder = new StringBuilder("This compound has a ");
		boolean iucr = property.startsWith("iucr:");
		if (iucr) {
			builder.append("<a href='");
			builder.append(IUCRdictURL);
			builder.append(property.substring(5));
			builder.append("'>");
		}
		builder.append(property);
		if (iucr)
			builder.append("</a>");
		builder.append(" of ");
		builder.append(this.outputMap.get(root));
		builder.append(" which is ");
		builder.append(comparator);
		builder.append(value);
		return builder.toString();
	}

	public ValueFilter(String property, String value, String comparator) {
		this.property = property;
		this.value = value;
		this.comparator = comparator;
		init();
	}

	public ValueFilter(Configuration conf) {
		this.property = conf.filterProperty;
		this.value = conf.filterValue;
		this.comparator = conf.filterComparator;
		init();
	}

	private void init() {
		context = new XPathContext();
		context.addNamespace("cml", CMLConstants.CML_NS);
		context.addNamespace(CifDictionaryBuilder.PREFIX,
				CifDictionaryBuilder.URI);
		xpath = "//cml:property[@dictRef='" + property + "']";
		outputMap = new LinkedHashMap<CMLElement, String>();
	}

	public Map<CMLElement, String> getResultsMap() {
		return this.outputMap;
	}

	protected List<Comparable> getMatchingValues(CMLElement root) {
		List<Comparable> results = new ArrayList<Comparable>();
		Nodes nodes = root.query(xpath, context);
		for (int x = 0; x < nodes.size(); x++) {
			Element element = (Element) nodes.get(x);
			Elements scalars = element.getChildElements("scalar",
					CMLConstants.CML_NS);
			for (int y = 0; y < scalars.size(); y++) {
				CMLScalar scalar = (CMLScalar) scalars.get(y);
				String value = scalar.getValue();
				setType(scalar.getAttributeValue("dataType"));
				Comparable fileValue = parseStringToType(value);
				Comparable target = parseStringToType(this.value);
				switch (this.comparator.charAt(0)) {
				case '<':
					if (fileValue.getClass().equals(Double.class)) {
						double fileDouble = (Double) fileValue;
						double targetDouble = (Double) target;
						if (this.comparator.length() > 1
								&& '=' == this.comparator.charAt(1)) {
							if (fileDouble <= targetDouble) {
								results.add(fileValue);
							}
						} else if (fileDouble < targetDouble) {
							results.add(fileValue);
						}
					} else if (fileValue.getClass().equals(Integer.class)) {
						int fileInt = (Integer) fileValue;
						int targetInt = (Integer) target;
						if (this.comparator.length() > 1
								&& '=' == this.comparator.charAt(1)) {
							if (fileInt <= targetInt) {
								results.add(fileValue);
							}
						} else if (fileInt < targetInt) {
							results.add(fileValue);
						}
					}
					break;
				case '>':
					if (fileValue.getClass().equals(Double.class)) {
						double fileDouble = (Double) fileValue;
						double targetDouble = (Double) target;
						if (this.comparator.length() > 1
								&& '=' == this.comparator.charAt(1)) {
							if (fileDouble >= targetDouble) {
								results.add(fileValue);
							}
						} else if (fileDouble > targetDouble) {
							results.add(fileValue);
						}
					} else if (fileValue.getClass().equals(Integer.class)) {
						int fileInt = (Integer) fileValue;
						int targetInt = (Integer) target;
						if (this.comparator.length() > 1
								&& '=' == this.comparator.charAt(1)) {
							if (fileInt <= targetInt) {
								results.add(fileValue);
							}
						} else if (fileInt > targetInt) {
							results.add(fileValue);
						}
					}
					break;
				case '=':
					if (fileValue.getClass().equals(Integer.class)) {
						int fileInt = (Integer) fileValue;
						int targetInt = (Integer) target;
						if (fileInt == targetInt) {
							results.add(fileValue);
						}
					} else if (fileValue.getClass().equals(String.class)) {
						String fileString = (String) fileValue;
						String targetString = (String) target;
						if (fileString != null
								&& fileString.equals(targetString)) {
							results.add(fileValue);
						}
					}
					break;
				default:
					break;
				}

			}
		}
		return results;
	}

	private void setType(String type) {
		if ("xsd:double".equals(type)) {
			this.type = Double.class;
		} else if ("xsd:string".equals(type)) {
			this.type = String.class;
		} else if ("xsd:integer".equals(type)) {
			this.type = Integer.class;
		}
	}

	private Comparable parseStringToType(String string) {
		Comparable parse = null;
		if (String.class.equals(type)) {
			return string;
		} else if (Double.class.equals(type)) {
			parse = new Double(Double.parseDouble(string));
			return parse;
		} else if (Integer.class.equals(type)) {
			parse = new Integer(Integer.parseInt(string));
			return parse;
		}
		// If xsd:dateType hasn't been set, try to parse the string anyway.
		try {
			parse = new Double(Double.parseDouble(string));
		} catch (NumberFormatException e) {
			try {
				parse = new Integer(Integer.parseInt(string));
			} catch (NumberFormatException e2) {
				parse = string;
			}
		}
		return parse;
	}

	public void addAllCmlElement(List<CMLElement> list) {
		for (CMLElement root : list) {
			this.addCMLElement(root);
		}
	}

	public void addCMLElement(CMLElement root) {
		List<Comparable> query = getMatchingValues(root);
		if (query.size() > 0) {
			Collections.sort(query);
			StringBuilder str = new StringBuilder();
			for (Comparable a : query) {
				str.append(a.toString());
				str.append(", ");
			}
			String values = str.substring(0, str.length() - 2);
			outputMap.put(root, values);
			// TODO store values, post message with info about it
		}
	}

}
