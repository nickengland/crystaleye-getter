package cam.nwe23.io;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import nu.xom.Document;
import nu.xom.ParsingException;
import nu.xom.ValidityException;

import org.ccil.cowan.tagsoup.Parser;
import org.xml.sax.SAXException;


public class Builder {
	
	nu.xom.Builder xomBuilder;
	
	public Document build(InputStream in) throws ParsingException,
			ValidityException, IOException {
		return xomBuilder.build(in);
	}

	public Document build(File in) throws ParsingException, ValidityException,
			IOException {
		return xomBuilder.build(in);
	}

	public Builder(){
		Parser tagSoup = new Parser();
		nu.xom.Builder builder = new nu.xom.Builder(tagSoup);
		this.xomBuilder=builder;
	}
}
