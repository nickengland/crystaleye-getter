package cam.nwe23.io;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import nu.xom.Document;
import nu.xom.Element;
import nu.xom.Nodes;
import nu.xom.ParsingException;
import nu.xom.ValidityException;

import org.xmlcml.cml.base.CMLBuilder;
import org.xmlcml.cml.base.CMLConstants;
import org.xmlcml.cml.element.CMLMolecule;

import cam.nwe23.descriptors.DescriptorMinMax;
import cam.nwe23.descriptors.DescriptorValues;

public class ReadFiles {

	List<File> files;
	DescriptorMinMax minMax=new DescriptorMinMax();

	public ReadFiles(File dir) {
		readDir(dir);
		init();
	}

	private void readDir(File dir) {
		if(!dir.isDirectory()){
			throw new IllegalArgumentException(dir+" is not a directory");
		}
		File[] files=dir.listFiles();
		this.files=Arrays.asList(files);
	}

	public ReadFiles(List<File> files) {
		this.files = files;
		init();
	}

	private void init() {
		this.calcMinMaxDescriptors();
	}
	
	protected void calcMinMaxDescriptors(){
		for(File file:files){
			CMLMolecule mol=null;
			CMLBuilder builder = new CMLBuilder();
			try {
				Document doc=builder.build(file);
				Element root=doc.getRootElement();
				Nodes nodes=root.query("//cml:molecule", CMLConstants.CML_XPATH);
				if(nodes.size()>=1){
					mol=(CMLMolecule)nodes.get(nodes.size()-1);
				}
			} catch (ValidityException e) {
				e.printStackTrace();
				continue;
			} catch (ParsingException e) {
				e.printStackTrace();
				continue;
			} catch (IOException e) {
				e.printStackTrace();
				continue;
			}
			DescriptorValues values = new DescriptorValues(mol);
			minMax.addValue(values);
		}
	}
	
	public DescriptorMinMax getMinMax(){
		return this.minMax;
	}

//	public static Document getHTMLForMol(CMLMolecule mol){
//		
//	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
