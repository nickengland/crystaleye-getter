package cam.nwe23.main;

import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpHost;
import org.apache.http.client.HttpClient;
import org.apache.http.conn.params.ConnRoutePNames;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.log4j.Logger;

import uk.ac.cam.ch.wwmm.httpcrawler.HttpCrawler;
import uk.ac.cam.ch.wwmm.httpcrawler.cache.HttpCache;
import uk.ac.cam.ch.wwmm.httpcrawler.cache.mongo.MongoCache;
import wwmm.pubcrawler.CrawlerContext;
import wwmm.pubcrawler.crawlers.AbstractCrawlerFactory;
import wwmm.pubcrawler.crawlers.JournalHandler;
import wwmm.pubcrawler.crawlers.PubCrawler;
import wwmm.pubcrawler.crawlers.acta.ActaCrawlerFactory;
import wwmm.pubcrawler.data.mongo.MongoStore;
import wwmm.pubcrawler.journals.ActaInfo;
import wwmm.pubcrawler.model.Journal;

import cam.nwe23.config.Configuration;
import cam.nwe23.config.ConfigurationLoader;

import com.mongodb.DB;
import com.mongodb.Mongo;

public class ActaECrawler {

	private static final Logger LOG = Logger.getLogger(Main.class);
	public static Configuration conf;

	public static void main(String[] args) throws UnknownHostException {
		ConfigurationLoader loader = new ConfigurationLoader(args[0]);
		conf=loader.getConfiguration();

		Mongo mongo = new Mongo(conf.mongoURL);
		DB db = mongo.getDB(conf.dataBaseName);

		List<Journal> actaEOnly = new ArrayList<Journal>(1);
		actaEOnly.add(ActaInfo.getJournals().get(4));
		Crawler actaCrawler = createCrawler("IUCR", db,
				new ActaCrawlerFactory(), actaEOnly);

		actaCrawler.start();
	}

	private static Crawler createCrawler(String name, DB db,
			AbstractCrawlerFactory crawlerFactory, List<Journal> index) {
		HttpCrawler crawler = getCrawler(db);
		MongoStore store = new MongoStore(db);
		CrawlerContext context = new CrawlerContext(store, crawler,
				crawlerFactory);
		return new Crawler(name, index, context);
	}

	static class Crawler extends Thread {

		List<Journal> index;
		CrawlerContext context;

		Crawler(String name, List<Journal> index, CrawlerContext context) {
			super(name);
			this.index = index;
			this.context = context;
		}

		@Override
		public void run() {
			for (int i = 0; i < 2; i++) {
				for (Journal journal : index) {
					LOG.info("Crawling [" + i + "] : " + journal.getTitle());
					try {
						JournalHandler handler = context.getCrawlerFactory()
								.createJournalCrawler(journal, context);
						PubCrawler crawler = new PubCrawler(journal, handler,
								context);
						crawler.setMaxArticlesPerIssue(0);
						crawler.setMinYear(1990);
						crawler.run();
					} catch (Exception e) {
						LOG.error("Error crawling " + journal.getTitle(), e);
					}
				}
			}

		}

	}

	private static HttpCrawler getCrawler(DB db) {
		int connectionTimeoutMillis = 10000;
		int socketTimeoutMillis = 10000;

		HttpClient client = new DefaultHttpClient();
		HttpConnectionParams.setConnectionTimeout(client.getParams(),
				connectionTimeoutMillis);
		HttpConnectionParams.setSoTimeout(client.getParams(),
				socketTimeoutMillis);
		HttpProtocolParams.setUserAgent(client.getParams(), conf.userAgent);

		if (conf.proxyURL != null) {
			HttpHost proxy = new HttpHost(conf.proxyURL, conf.proxyPort);
			client.getParams().setParameter(ConnRoutePNames.DEFAULT_PROXY,
					proxy);
		}
		HttpCache cache = new MongoCache(db, conf.httpCacheName);
		HttpCrawler httpCrawler = new HttpCrawler(client, cache);
		return httpCrawler;
	}

}
