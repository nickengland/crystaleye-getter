package cam.nwe23.main;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.management.openmbean.InvalidOpenTypeException;

import nu.xom.Document;
import nu.xom.Element;
import nu.xom.Elements;
import nu.xom.Nodes;
import nu.xom.ParsingException;
import nu.xom.Serializer;
import nu.xom.ValidityException;

import org.apache.commons.io.IOUtils;
import org.xmlcml.cml.base.CMLConstants;
import org.xmlcml.cml.base.CMLElement;
import org.xmlcml.cml.element.CMLMolecule;

import redstone.xmlrpc.XmlRpcException;
import redstone.xmlrpc.XmlRpcFault;
import wwmm.pubcrawler.CrawlerContext;
import wwmm.pubcrawler.crawlers.acta.ActaArticleCrawler;
import wwmm.pubcrawler.model.Article;
import wwmm.pubcrawler.model.Issue;
import wwmm.pubcrawler.model.id.ArticleId;
import wwmm.pubcrawler.types.Doi;
import cam.nwe23.config.Configuration;
import cam.nwe23.config.ConfigurationLoader;
import cam.nwe23.descriptors.DescriptorTypes;
import cam.nwe23.descriptors.DescriptorValues;
import cam.nwe23.descriptors.MinMaxFilter;
import cam.nwe23.descriptors.ValueFilter;
import cam.nwe23.io.Builder;
import cam.nwe23.mongo.MongoFetcher;
import cam.nwe23.spider.ActaEResolver;
import cam.nwe23.wordpress.PostFormatter;
import cam.nwe23.wordpress.PostToWordpress;
import de.schlichtherle.truezip.file.TFile;
import de.schlichtherle.truezip.file.TFileInputStream;

public class Main {

	/**
	 * @param args
	 * @throws IOException
	 * @throws ParsingException
	 * @throws ValidityException
	 */
	MongoFetcher fetcher;
	ActaArticleCrawler crawler;
	CrawlerContext context;

	public static void main(String[] args) throws ValidityException,
			ParsingException, IOException, XmlRpcException, XmlRpcFault {
		String pathToConfig = "default.properties";
		if (args.length == 0) {
			File defaultFile = new File("default.properties");
			if (defaultFile.exists()) {
				System.out.println("Using default.properties file");
			} else {
				System.out
						.println("Usage: java -jar crystalblog.jar file.properties (spider)");
				System.out.println("Specify a properties file to use. Add \"spider\" to attempt to download the latest ActaE");
				System.out
						.println("Writing default properties to default.properties");
				IOUtils.copy(ConfigurationLoader.class
						.getResourceAsStream("/config.properties"),
						new FileOutputStream(defaultFile));
				System.exit(0);
			}
		}
		else if (args.length == 1) {
			pathToConfig = args[0];
		} else if(args.length==2){
			pathToConfig=args[0];
			if("spider".equals(args[1])){
				ActaECrawler.main(args);
			}
		}
		else{
			System.out.println("Should specify only one or two arguments");
			System.exit(0);
		}
		System.out.println("Initialising...");
		Main main = new Main();
		ConfigurationLoader loader = new ConfigurationLoader(pathToConfig);
		Configuration conf = loader.getConfiguration();
		main.newFetcher(conf);
		Issue issue = main.fetcher.getLatestActaE();
		if (issue == null) {
			System.out.println("No actaE is in the mongo database "
					+ conf.mongoURL);
			System.exit(1);
		}
		System.out.println("Latest ActaE is:"+" "+issue.getVolume()+" "+issue.getNumber());
		System.out.println("Found " + issue.getArticles().size() + " articles");
		List<CMLElement> cmls = main.getCifsFromArticles(issue.getArticles());
		System.out.println("Found " + cmls.size() + " structures");
		System.out.println("Starting filtering...");
		if ("value".equals(conf.filterType)) {
			ValueFilter filter = new ValueFilter(conf);
			filter.addAllCmlElement(cmls);
			System.out.println("Filtering complete");
			for (CMLElement root : filter.getResultsMap().keySet()) {
				Element blurb = getSummary(root);
				PostFormatter formatter = new PostFormatter(conf,root,
						blurb, filter.getReasonString(root));
				PostToWordpress post = formatter.createPostToWordpress();
				post.addCategory(conf.filterProperty);
				post.post(conf.postImmediately);
			}
		} else {
			MinMaxFilter filter = new MinMaxFilter();
			filter.addAllCmlElement(cmls);
			System.out.println("Filtering complete");
			System.out.println("Posting to wordpress");
			for (DescriptorTypes type : DescriptorTypes.values()) {
				DescriptorValues value = filter.getMaxValue(type);
				Element blurb = getSummary(value);
				PostFormatter poster = new PostFormatter(conf,value.getRootElement(), blurb,
						"This compound has the maximum " + type
								+ " in issue, with value: "
								+ value.getValueMap().get(type));
				poster.createPostToWordpress().post(conf.postImmediately);
			}
		}
		System.out.println("Finished posting to " + conf.wordpressXmlRpcURL);
	}

	private static Element getSummary(CMLElement root)
			throws FileNotFoundException, ParsingException, ValidityException,
			IOException {
		ActaEResolver resolver = new ActaEResolver();
		TFile summary = resolver.getIfCached(getDoi(root));
		Builder builder = new Builder();
		InputStream stream = new TFileInputStream(summary);
		Document doc = builder.build(stream);
		Element blurb = doc.getRootElement();
		return blurb;
	}

	private static Element getSummary(DescriptorValues value)
			throws FileNotFoundException, ParsingException, ValidityException,
			IOException {
		return getSummary(value.getRootElement());
	}

	private static void debug(DescriptorValues value)
			throws FileNotFoundException, IOException {
		Serializer ser = new Serializer(new FileOutputStream(new File("test"
				+ UUID.randomUUID() + ".cml")));
		value.getRootElement().detach();
		Document docTest = new Document(value.getRootElement());
		ser.write(docTest);
		ser.flush();
	}

	private static String getDoi(CMLElement root) {
		Elements childen = root.getChildCMLElements("scalar");
		for (int x = 0; x < childen.size(); x++) {
			Element elem = childen.get(x);
			if ("idf:doi".equals(elem.getAttributeValue("dictRef"))) {
				return elem.getValue();
			}
		}
		return null;
	}

	private static String getDoi(DescriptorValues value) {
		return getDoi(value.getRootElement());
	}

	public void newFetcher(Configuration conf) {
		this.fetcher = new MongoFetcher(conf);
	}

	private List<CMLElement> getCifsFromArticles(List<Article> articles) {
		List<CMLElement> results = new ArrayList<CMLElement>();
		for (Article article : articles) {
			List<CMLElement> elementList = fetcher
					.getCifsFromArticleAsCML(article);
			results.addAll(elementList);
		}
		return results;
	}

	private List<CMLElement> getCifsAsCML(List<Doi> dois) {
		List<CMLElement> totalList = new ArrayList<CMLElement>();
		for (Doi doi : dois) {
			System.out.println(doi);
			// TODO Address issues being not downloaded yet.
			List<CMLElement> elementList = fetcher.getCifsFromDoiAsCML(doi);
			totalList.addAll(elementList);
		}
		return totalList;
	}



}
