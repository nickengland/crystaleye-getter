package cam.nwe23.mongo;

import java.io.IOException;
import java.io.InputStream;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import nu.xom.Element;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpHost;
import org.apache.http.client.HttpClient;
import org.apache.http.conn.params.ConnRoutePNames;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpProtocolParams;
import org.joda.time.Duration;
import org.xmlcml.cml.base.CMLAttribute;
import org.xmlcml.cml.base.CMLElement;
import org.xmlcml.cml.converters.cif.CIF2CIFXMLConverter;
import org.xmlcml.cml.converters.cif.CIFXML2CMLConverter;
import org.xmlcml.cml.converters.cif.RawCML2CompleteCMLConverter;
import org.xmlcml.cml.element.CMLMolecule;
import org.xmlcml.cml.element.CMLScalar;

import uk.ac.cam.ch.wwmm.httpcrawler.CrawlerGetRequest;
import uk.ac.cam.ch.wwmm.httpcrawler.CrawlerResponse;
import uk.ac.cam.ch.wwmm.httpcrawler.HttpCrawler;
import uk.ac.cam.ch.wwmm.httpcrawler.cache.HttpCache;
import uk.ac.cam.ch.wwmm.httpcrawler.cache.mongo.MongoCache;
import wwmm.pubcrawler.CrawlerContext;
import wwmm.pubcrawler.crawlers.acta.ActaArticleCrawler;
import wwmm.pubcrawler.crawlers.acta.ActaCrawlerFactory;
import wwmm.pubcrawler.data.mongo.MongoStore;
import wwmm.pubcrawler.model.Article;
import wwmm.pubcrawler.model.Issue;
import wwmm.pubcrawler.model.Journal;
import wwmm.pubcrawler.model.SupplementaryResource;
import wwmm.pubcrawler.model.id.ArticleId;
import wwmm.pubcrawler.model.id.IssueId;
import wwmm.pubcrawler.model.id.JournalId;
import wwmm.pubcrawler.types.Doi;

import cam.nwe23.config.Configuration;
import cam.nwe23.config.ConfigurationLoader;

import com.mongodb.DB;
import com.mongodb.Mongo;
import com.mongodb.MongoException;

public class MongoFetcher {
	Configuration conf;
	Mongo mongo;
	DB db;
	HttpCrawler crawler;
	MongoStore store;
	CrawlerContext context;
	CIF2CIFXMLConverter conv = new CIF2CIFXMLConverter();
	CIFXML2CMLConverter conv2 = new CIFXML2CMLConverter();
	RawCML2CompleteCMLConverter conv3 = new RawCML2CompleteCMLConverter();

	public MongoFetcher(){
		try {
			init(ConfigurationLoader.getStaticConfiguartion());
		} catch (UnknownHostException e) {
			e.printStackTrace();
			throw new RuntimeException("Couldn't contact host", e);
		} catch (MongoException e) {
			e.printStackTrace();
			throw new RuntimeException("MongoException", e);
		}
	}
	
	public MongoFetcher(Configuration conf) {
		try {
			init(conf);
		} catch (UnknownHostException e) {
			throw new RuntimeException("Couldn't contact host", e);
		} catch (MongoException e) {
			throw new RuntimeException("MongoException", e);
		}
	}

	void init(Configuration conf) throws UnknownHostException, MongoException {
		this.conf=conf;
		mongo = new Mongo(conf.mongoURL);
		db = mongo.getDB("pubcrawler");
		crawler = getCrawler(db);
		store = new MongoStore(db);
		context = new CrawlerContext(store, crawler, new ActaCrawlerFactory());
	}

	private HttpCrawler getCrawler(DB db) {
		int connectionTimeoutMillis = 10000;
		int socketTimeoutMillis = 10000;

		HttpClient client = new DefaultHttpClient();
		HttpConnectionParams.setConnectionTimeout(client.getParams(),
				connectionTimeoutMillis);
		HttpConnectionParams.setSoTimeout(client.getParams(),
				socketTimeoutMillis);
		HttpProtocolParams.setUserAgent(client.getParams(), conf.userAgent);

		if(conf.proxyURL!=null){
			HttpHost proxy = new HttpHost(conf.proxyURL, conf.proxyPort);
			client.getParams().setParameter(ConnRoutePNames.DEFAULT_PROXY, proxy);
		}

		HttpCache cache = new MongoCache(db, "httpcache");
		HttpCrawler httpCrawler = new HttpCrawler(client, cache);
		return httpCrawler;
	}

	public Article getArticleFromDoi(Doi doi) {
		Article article=this.store.findArticleByDoi(doi);
		return article;
	}

	public List<CMLElement> getCifsFromDoiAsCML(Doi doi) {
		return this.getCifsFromArticleAsCML(this.getArticleFromDoi(doi));
	}

	public Issue getLatestActaE(){
		Journal actae=this.store.findJournal(new JournalId("acta/e"));
		if(actae==null){
			System.err.println("No acta/e journal in this mongoDB");
			return null;
		}
		Issue latest = this.getLatestIssue(actae);
		return latest;
	}
	
	public Issue getLatestIssue(Journal journal) {
		List<String> issues=(List<String>) journal.get("issues");
		Collections.sort(issues);
		Issue issue=this.store.findIssue(new IssueId(issues.get(issues.size()-1)));
		return issue;
	}
	
	public List<CMLElement> getCifsFromArticleAsCML(Article article) {
		if(article==null){
			throw new IllegalArgumentException("Article cannot be null");
		}
		Doi doi = article.getDoi();
		List<CMLElement> output = new ArrayList<CMLElement>();
		List<SupplementaryResource> list = article.getSupplementaryResources();
		for (SupplementaryResource res : list) {
			if ("chemical/x-cif".equals(res.getContentType())) {
				String id = article.getId() + "/" + res.getFilePath();
				CrawlerGetRequest request = new CrawlerGetRequest(res.getUrl(),
						id, new Duration(Long.MAX_VALUE));
				CrawlerResponse response = null;
				try {
					response = this.crawler.execute(request);
					InputStream stream = response.getContent();
					
					Element cifxml=conv.convertToXML(IOUtils.readLines(stream));
					Element rawcml = conv2.convertToXML(cifxml);
					CMLElement cml = (CMLElement) conv3.convertToXML(rawcml);
					CMLScalar doiScalar = new CMLScalar(doi.getUrl().toString());
					doiScalar.addAttribute(new CMLAttribute("dictRef", "idf:doi"));
					cml.appendChild(doiScalar);
					output.add(cml);
				} catch (IOException e) {
					e.printStackTrace();
					throw new RuntimeException(e);
				}

			}
		}
		return output;
	}
}
