package cam.nwe23.spider;

import java.io.IOException;

import org.restlet.data.Response;
import org.restlet.data.Status;

import de.schlichtherle.truezip.file.TFile;
import de.schlichtherle.truezip.file.TFileWriter;

public class ActaEResolver {

	TFile downloadedStore = new TFile("download.zip");

	public CifBundle resolveDoi(String doi) {
		CifBundle bundle = new CifBundle();
		bundle.doi = doi;
		bundle.summary = getIfCached(doi);

		return bundle;
	}

	public TFile getIfCached(String doi) {
		TFile file = PageGetter.getFile(doi);
		if (file.exists()) {
			return file;
		} else {
			try {
				return download(doi);
			} catch (IOException e) {
				e.printStackTrace();
				return null;
			}
		}
	}
	
//	private void getCIF(String doi){
//		CrawlerContext context= new CrawlerContext(dataStore, new HttpCrawler()), new ActaCrawlerFactory());
//		Article article = new Article();
//		article.setDoi(new Doi(doi));
//		ActaArticleCrawler crawl = new ActaArticleCrawler(article, context);
//	}
	
	
	private TFile download(String doi) throws IOException {
		TFile file=null;
		CIFRequest req= new CIFRequest(doi);
        Response response = req.getResponse();
        if (response.getStatus().isSuccess()) {
        	file=PageGetter.getFile(doi);
        	TFileWriter writer = new TFileWriter(file);
            response.getEntity().write(writer);
        	writer.close();
            return file;
        } else if (response.getStatus().equals(
                Status.CLIENT_ERROR_UNAUTHORIZED)) {
            // Unauthorised access
            System.out.println("Access unauthorized by the server, "
                    + "check your credentials");
        } else {
            // Unexpected status
            System.out.println("An unexpected status was returned: "
                    + response.getStatus() + " for id " + doi);
        }
        return file;
	}
}
