package cam.nwe23.spider;

import java.util.List;

import org.restlet.Client;
import org.restlet.Context;
import org.restlet.data.ClientInfo;
import org.restlet.data.Method;
import org.restlet.data.Parameter;
import org.restlet.data.Protocol;
import org.restlet.data.Request;
import org.restlet.data.Response;
import org.restlet.util.Series;

public class CIFRequest {

    String url;

    Response response;

    public CIFRequest(String url) {
        init();
        this.url = url;
    }
    
    
    Series<Parameter> parameters=new Series<Parameter>() {
        
        @Override
        public Parameter createEntry(String name, String value) {
            return(new Parameter(name, value));
        }

        @Override
        public Series<Parameter> createSeries(List<Parameter> delegate) {
            //TODO NYI
            return null;
        }
    };
    
   protected CIFRequest() {
        init();
    }
    
    private void init(){
        Parameter param = new Parameter("followRedirects", "true");
        parameters.add(param);
    }

   public Response getResponse() {
        if (response != null)
            return response;
        else {
            System.out.println("Getting " + url);
            // Prepare the request
            Request request = new Request(Method.GET, url);

            // Ask to the HTTP client connector to handle the call
            Client client = new Client(Protocol.HTTP);
            Context context=new Context();
            context.setParameters(parameters);
            client.setContext(context);
            ClientInfo info = new ClientInfo();
            info.setAgent("Auto-CIF downloaderthingy");
            request.setClientInfo(info);

            response = client.handle(request);
            return response;
        }
    }

}