package cam.nwe23.spider;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import nu.xom.Builder;
import nu.xom.Document;
import nu.xom.Nodes;
import nu.xom.ParsingException;
import nu.xom.ValidityException;

import org.ccil.cowan.tagsoup.Parser;
import org.restlet.data.Response;

public class IssuePage {
	CIFRequest request;
	Response response;
	Map<String, String> dois;
	
	public IssuePage(String string) throws ValidityException, ParsingException, IOException {
		request=new CIFRequest(string);
		response=request.getResponse();
		parseResponse();
	}

	private void parseResponse() throws ValidityException, ParsingException, IOException {
		Parser tagSoup = new Parser();
		Builder builder = new Builder(tagSoup);
		Document doc=builder.build(response.getEntity().getStream());
		Nodes rows=doc.query("/x:html/x:body/x:div[@id=\"content\"]/x:table/x:tr", Constants.XHTML);
		System.out.println(rows.size()+" rows");
		this.dois=new LinkedHashMap<String, String>(rows.size());
		for(int x=0;x<rows.size();x++){
			Nodes links=rows.get(x).query("./x:td/x:a[@style]/@href", Constants.XHTML);
			if(links.size()!=2){
				continue;
			}
			for(int y=0;y<links.size();y++){
				dois.put(links.get(0).getValue(),links.get(1).getValue());
			}
		}
	}

	public Set<String> getDois() {
		return this.dois.keySet();
	}
	public String getCMLforDoi(String doi){
		return this.dois.get(doi);
	}
	
}
