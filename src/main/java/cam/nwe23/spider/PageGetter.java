package cam.nwe23.spider;

import java.io.IOException;
import java.util.Random;

import nu.xom.ParsingException;
import nu.xom.ValidityException;

import org.restlet.data.Response;
import org.restlet.data.Status;

import de.schlichtherle.truezip.file.TFile;
import de.schlichtherle.truezip.file.TFileWriter;

public class PageGetter {

        private static String folderName = "download.zip";
		private static Random rng=new Random();
		private static IssuePage page;
		private static int downloaded = 0;

		public static void main(String[] args) throws IOException,
                InterruptedException, ValidityException, ParsingException {
            rng.setSeed(System.currentTimeMillis());
            TFile folder = new TFile(folderName);
            folder.mkdir();
            page = new IssuePage("http://wwmm.ch.cam.ac.uk/crystaleye/summary/acta/e/2008/01-00/index.html");
            downloadDois();
            System.out.println("Finished! " + downloaded + " files downloaded");
        }


		  public static String getFileName(String doi){
		        StringBuilder builder = new StringBuilder();
		        for(int x=18;x<doi.length();x++){
		            char c=doi.charAt(x);
		            switch (c) {
                    case '/':
                        builder.append('_');
                        break;
                    default:
                        builder.append(c);
                        break;
                    }
		        }
		        return builder.toString();
		        
		    }
		  public static TFile getFile(String doi){
		  TFile download = new TFile(folderName + TFile.separator + getFileName(doi)
                  + ".html");
		  return download;
		  }
		  
		public static void downloadDois() throws InterruptedException,
				IOException {
			for (String doi :page.getDois()) {
                TFile download = getFile(doi);
                if (download.exists()) {
                    System.out.println("found " + getFileName(doi));
                    continue;
                }
                CIFRequest cif = new CIFRequest(doi);
                Response response = cif.getResponse();
                do {
                    Thread.sleep((long)(1000*(rng.nextFloat()+0.5)));
                } while (rng.nextBoolean() == false);
                if (response.getStatus().isSuccess()) {
                    response.getEntity().write(new TFileWriter(getFile(doi)));
                    downloaded++;
                } else if (response.getStatus().equals(
                        Status.CLIENT_ERROR_UNAUTHORIZED)) {
                    // Unauthorised access
                    System.out.println("Access unauthorized by the server, "
                            + "check your credentials");
                } else {
                    // Unexpected status
                    System.out.println("An unexpected status was returned: "
                            + response.getStatus() + " for id " + doi);
                }
            }
		}
    }
