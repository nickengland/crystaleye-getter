package cam.nwe23.wordpress;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import nu.xom.Document;
import nu.xom.Serializer;

import org.apache.commons.io.FileUtils;
import org.xmlcml.cml.base.CMLElement;
import org.xmlcml.cml.element.CMLCml;
import org.xmlcml.cml.element.CMLMolecule;

import cam.nwe23.config.Configuration;
import cam.nwe23.config.ConfigurationLoader;

import redstone.xmlrpc.XmlRpcClient;
import redstone.xmlrpc.XmlRpcException;
import redstone.xmlrpc.XmlRpcFault;
import redstone.xmlrpc.XmlRpcStruct;
import sea36.image.jmol.ImageGeneratorJmol;

public class ImageUploader {

	XmlRpcClient client;
	String url;
	Map<String, Object> fileData;
	String username;
	String password;

	public void init(Configuration conf) throws MalformedURLException {
		this.url = conf.wordpressXmlRpcURL;
		this.username = conf.wordpressUsername;
		this.password = conf.wordpressPassword;
		fileData = new HashMap<String, Object>();
		URL url = new URL(this.url);
		client = new XmlRpcClient(url, false);
	}

	public ImageUploader(Configuration conf) {
		try {
			this.init(conf);
		} catch (MalformedURLException e) {
			System.err
					.println("wordpressXmlRpcURL in config causes MalformedURLException!");
			e.printStackTrace();
			throw new IllegalStateException(url + "is malformed URL", e);
		}
	}

	@Deprecated
	public ImageUploader() {
		Configuration conf = ConfigurationLoader.getStaticConfiguartion();
		try {
			this.init(conf);
		} catch (MalformedURLException e) {
			System.err
					.println("hard-coded string should not throw MalformedURLException!");
			e.printStackTrace();
			throw new IllegalStateException(
					"hard-coded string should not throw MalformedURLException!",
					e);
		}
	}

	public XmlRpcStruct uploadImage(File image, String name)
			throws IOException, XmlRpcException, XmlRpcFault {
		fileData.put("name", name);
		fileData.put("type", "image/png");
		fileData.put("bits", open(image));
		fileData.put("overwrite", Boolean.TRUE);
		Object[] params = new Object[] { new Integer(0), username, password,
				fileData };
		XmlRpcStruct uploadResult = (XmlRpcStruct) client.invoke(
				"metaWeblog.newMediaObject", params);
		return uploadResult;
	}
	

	Object open(File image) throws IOException {
		return FileUtils.readFileToByteArray(image);
	}

	public static File storeMolAsTempFile(CMLElement mol) throws IOException {
		File result = File.createTempFile("mol", ".cml");
		if (mol.getId() == null) {
			mol.setId(result.getName().substring(0,
					result.getName().length() - 5));
		}
		Serializer ser = new Serializer(new FileOutputStream(result));
		ser.setIndent(4);
		CMLCml cml = new CMLCml();
		cml.appendChild(mol);
		Document doc = new Document(cml);
		ser.write(doc);
		ser.flush();
		return result;
	}

	public static File getImageTempFile() throws IOException {
		File image = File.createTempFile("img", ".png");
		return image;
	}

	public static File createImage(File molFile, File imageFile) {
		ImageGeneratorJmol imageGen = new ImageGeneratorJmol();
		try {
			imageGen.loadMolecule(molFile);
			imageGen.setShowAxes(true);
			imageGen.setShowUnitCell(true);
			imageGen.createImage(imageFile, 300, 300);
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			imageGen.shutdown();
		}
		return imageFile;
	}

	public String generateAndUploadImage(CMLMolecule mol) {
		try {
			File imageFile = ImageUploader.createImage(
					ImageUploader.storeMolAsTempFile(mol),
					ImageUploader.getImageTempFile());
			String name = mol.getId() + ".png";
			if (mol.getId() == null) {
				name = "molecule.png";
			}
			XmlRpcStruct reply = this.uploadImage(imageFile, name);
			return reply.getString("url");
		} catch (Exception e) {
			throw new RuntimeException("Failed to generate image", e);
		}
	}

}
