package cam.nwe23.wordpress;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.LinkedHashMap;
import java.util.Map;

import nu.xom.Element;
import nu.xom.Elements;
import nu.xom.Node;
import nu.xom.Nodes;
import nu.xom.Text;

import org.apache.commons.io.IOUtils;
import org.xmlcml.cml.base.CMLConstants;
import org.xmlcml.cml.base.CMLElement;
import org.xmlcml.cml.element.CMLMolecule;

import cam.nwe23.config.Configuration;

public class PostFormatter {

	CMLMolecule mol;
	CMLElement root;
	Element blurb;
	Map<String, String> results;
	String htmlForPosting;
	String reason;
	String imageURL;
	String bibline;
	Configuration conf;

	public PostFormatter(Configuration conf, CMLElement root, Element blurb,
			String reason) throws IOException {
		if (root == null) {
			throw new IllegalArgumentException("root cannot be null");
		} else if (blurb == null) {
			throw new IllegalArgumentException("Blurb cannot be null");
		} else if (conf == null) {
			throw new IllegalArgumentException("Configuration must not be null");
		}
		this.conf = conf;
		this.root = root;
		this.mol=getMol(root);
		this.blurb = blurb;
		this.reason = reason;
		init();
	}
	
	@Deprecated
	public PostFormatter(Configuration conf, CMLMolecule mol, Element blurb, String reason)
			throws IOException {
		if (mol == null) {
			throw new IllegalArgumentException("mol cannot be null");
		} else if (blurb == null) {
			throw new IllegalArgumentException("Blurb cannot be null");
		}else if(conf==null){
			throw new IllegalArgumentException("Configuration must not be null");
		}
		this.conf=conf;
		this.mol = mol;
		this.blurb = blurb;
		this.reason = reason;
		init();
	}

	private void init() throws IOException {
		results = this.parseBlurb();
		try{
		generateImage(conf);
		}catch(RuntimeException e){
			this.imageURL=null;
		}
		htmlForPosting = this.createPostBody();
	}

	protected void generateImage(Configuration conf) {
		ImageUploader imageUploader = new ImageUploader(conf);
		this.imageURL=imageUploader.generateAndUploadImage(mol);
	}

	private static CMLMolecule getMol(CMLElement root) {
		CMLMolecule mol = null;
		Nodes nodes = root.query("//cml:molecule", CMLConstants.CML_XPATH);
		if (nodes.size() >= 1) {
			mol = (CMLMolecule) nodes.get(nodes.size() - 1);
		}
		return mol;
	}
	
	public PostToWordpress createPostToWordpress() throws MalformedURLException {
		PostToWordpress poster = new PostToWordpress(conf);
		poster.setTitle(this.results.get("title"));
		poster.setContent(this.htmlForPosting);
		poster.setCrystalography();
		return poster;
	}

	public PostToWordpress createPostToWordpress(String category) {
		PostToWordpress poster = new PostToWordpress();
		poster.setTitle(this.results.get("title"));
		poster.setContent(this.htmlForPosting);
		poster.addCategory(category);
		return poster;
	}

	public String getHTMLForPosting() {
		return htmlForPosting;
	}

	protected String createPostBody() {
		StringBuilder builder = new StringBuilder();
		builder.append(bibline);
		builder.append(reason);
		builder.append("<br/>");
		if (this.imageURL != null) {
			builder.append("<img src=\"" + this.imageURL + "\"alt=\""
					+ results.get("title") + "\"><br/>");
		}
		builder.append(results.get("content"));
		builder.append("<br/>");
		htmlForPosting = builder.toString();
		return builder.toString();
	}

	protected Map<String, String> parseBlurb() throws IOException {
		Map<String, String> results = new LinkedHashMap<String, String>();
		Elements elems = blurb.getChildElements("body", CMLConstants.XHTML_NS);
		Element body = elems.get(0);
		Elements titleElements = body.getChildElements("h3",
				CMLConstants.XHTML_NS);
		Element title = titleElements.get(2);
		Elements ps = body.getChildElements("p", CMLConstants.XHTML_NS);
		String contents = "";
		for (int x = 0; x < ps.size(); x++) {
			Element p = ps.get(x);
			replaceImagesWithAlt(p);
			contents = p.toXML();
			if (contents.contains("Abstract")) {
				break;
			}
		}
		Elements divs = body.getChildElements("div", CMLConstants.XHTML_NS);
		for (int x = 0; x < divs.size(); x++) {
			Element div = divs.get(x);
			if (div.getAttribute("class").getValue().equals("bibline")) {
				this.bibline = div.toXML();
			}
		}

		results.put("content", contents);
		results.put("title", stripTags(replaceImagesWithAlt(title).toXML()));
		return results;
	}
	
	public Element replaceImagesWithAlt(Element e){
		Elements children=e.getChildElements();
		for(int x=0;x<children.size();x++){
			Element child=children.get(x);
			if(child.getLocalName().equalsIgnoreCase("img")){
				String altText=child.getAttributeValue("alt");
				e.replaceChild(child, new Text(altText));
			}
		}
		return e;
	}
	
	public String stripTags(String s) {
		StringBuilder builder= new StringBuilder();
		boolean inTag=false;
		for (int x = 0; x < s.length(); x++) {
			char c=s.charAt(x);
			if (!inTag) {
				if ('<' == c) {
					inTag=true;
					continue;
				}
				else{
					builder.append(c);
				}
			} else {
				if('>'==c){
					inTag=false;
					continue;
				}

			}
		}
		return builder.toString();
	}

}
