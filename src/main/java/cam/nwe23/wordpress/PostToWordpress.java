package cam.nwe23.wordpress;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cam.nwe23.config.Configuration;
import cam.nwe23.config.ConfigurationLoader;

import redstone.xmlrpc.XmlRpcClient;
import redstone.xmlrpc.XmlRpcException;
import redstone.xmlrpc.XmlRpcFault;

public class PostToWordpress {

	XmlRpcClient client;
	String url;
	String newPost = "metaWeblog.newPost";
	String username;
	String password;
	Map<String, Object> contentMap = new HashMap<String, Object>();
	List<String> cats;

	public PostToWordpress(Configuration conf) throws MalformedURLException {
		this.init(conf);
	}
	
	public PostToWordpress(){
		try {
			this.init(ConfigurationLoader.getStaticConfiguartion());
		} catch (MalformedURLException e) {
			System.err.println(this.url+" is malformed");
			e.printStackTrace();
			throw new IllegalStateException(url+" is causing MalformedURLException!",e);
		}
	}

	private void init(Configuration configuration) throws MalformedURLException {
		this.url=configuration.wordpressXmlRpcURL;
		username=configuration.wordpressUsername;
		password=configuration.wordpressPassword;
		URL url = new URL(this.url);
		client = new XmlRpcClient(url, false);
		cats = new ArrayList<String>();
	}

	public String post(boolean publish) throws XmlRpcException, XmlRpcFault {
		Object reply = client.invoke(newPost, new Object[] { new Integer(1),
				username, password, contentMap, new Boolean(publish) });
		return (String) reply;
	}

	public void setTitle(String title) {
		this.contentMap.put("title", title);
	}

	
	public void setCrystalography(){
		this.addCategory("crystallography");
	}
	
	/*
	 *Note category MUST already be created on the server
	 */
	public void addCategory(String cat) {
		cats.add(cat);
		this.contentMap.put("categories", cats.toArray());
	}

	public void setContent(String content){
		this.contentMap.put("description", content);
	}

	public void setSetting(String setting, String value) {
		this.contentMap.put(setting, value);
	}
}
