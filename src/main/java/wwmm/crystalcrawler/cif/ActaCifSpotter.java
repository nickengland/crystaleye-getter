package wwmm.crystalcrawler.cif;

import wwmm.pubcrawler.model.SupplementaryResource;

/**
 * @author Sam Adams
 */
public class ActaCifSpotter implements CifSpotter {

    public boolean isCif(SupplementaryResource resource) {
        if (resource.getFilePath() != null) {
            return resource.getFilePath().toLowerCase().endsWith(".cif");
        }
        return false;
    }

}
