package wwmm.crystalcrawler.cif;

import wwmm.pubcrawler.model.SupplementaryResource;

public interface CifSpotter {

    boolean isCif(SupplementaryResource resource);

}
