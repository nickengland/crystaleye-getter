package cam.nwe23.config;

import org.junit.Assert;
import org.junit.Test;


public class TestConfigurationLoader {

	@Test
	public void testConstructor(){
		ConfigurationLoader loader = new ConfigurationLoader();
		Assert.assertNotNull(loader);
		Assert.assertNotNull(loader.props.get("wordpressXmlRpcURL"));
	}
	@Test
	public void testReading(){
		ConfigurationLoader loader = new ConfigurationLoader("noexist.properties");
		Assert.assertEquals("http://localhost:8000/wordpress/xmlrpc.php",loader.getProperties().getProperty("wordpressXmlRpcURL"));
	}
	
	@Test
	public void testGetStaticConfig(){
		Configuration conf=ConfigurationLoader.getStaticConfiguartion();
		Assert.assertNotNull(conf);
		Assert.assertEquals("http://localhost:8000/wordpress/xmlrpc.php", conf.wordpressXmlRpcURL);
	}
}
