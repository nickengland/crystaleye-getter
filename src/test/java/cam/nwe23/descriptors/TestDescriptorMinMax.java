package cam.nwe23.descriptors;

import java.io.File;
import java.io.IOException;

import junit.framework.Assert;

import nu.xom.Document;
import nu.xom.ParsingException;
import nu.xom.ValidityException;

import org.junit.Before;
import org.junit.Test;
import org.xmlcml.cml.base.CMLBuilder;
import org.xmlcml.cml.base.CMLConstants;
import org.xmlcml.cml.element.CMLMolecule;
import org.xmlcml.cml.tools.SMILESTool;

public class TestDescriptorMinMax {
	
CMLMolecule mol;
	
	@Before
	public void setUp() throws ValidityException, ParsingException, IOException {
		CMLBuilder builder = new CMLBuilder();
		Document doc=builder.build(new File("src/test/resources/testCML/zs2105sup1_I_ring-nuc-sprout-2_1_4.complete.cml.xml"));
		mol=(CMLMolecule)doc.getRootElement().query("//cml:molecule", CMLConstants.CML_XPATH).get(0);
	}
	
	@Test
	public void testConstructor() {
		DescriptorMinMax desc = new DescriptorMinMax();
		double value = desc.getMaxNumericValue(DescriptorTypes.NUMBEROFATOMS);
		Assert.assertEquals(Double.MIN_VALUE, value);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testAddingNullValue(){
		DescriptorMinMax desc = new DescriptorMinMax();
		desc.addValue(null);
	}
	@Test
	public void testAddingNonNull(){
		DescriptorMinMax desc = new DescriptorMinMax();
		DescriptorValues values = new DescriptorValues(mol);
		desc.addValue(values);
		Assert.assertEquals(values.getMap().get(DescriptorTypes.NUMBEROFATOMS),desc.getMaxNumericValue(DescriptorTypes.NUMBEROFATOMS));
	}
	
}
