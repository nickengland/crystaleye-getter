package cam.nwe23.descriptors;

import java.io.File;
import java.io.IOException;

import junit.framework.Assert;

import nu.xom.Document;
import nu.xom.ParsingException;
import nu.xom.ValidityException;

import org.junit.Before;
import org.junit.Test;
import org.xmlcml.cml.base.CMLBuilder;
import org.xmlcml.cml.base.CMLConstants;
import org.xmlcml.cml.base.CMLElement;
import org.xmlcml.cml.element.CMLMolecule;
import org.xmlcml.cml.tools.SMILESTool;

public class TestDescriptorTypes {

	
CMLMolecule mol;
CMLElement root;
	
	@Before
	public void setUp() throws ValidityException, ParsingException, IOException {
		CMLBuilder builder = new CMLBuilder();
		Document doc=builder.build(new File("src/test/resources/testCML/zs2105sup1_I_ring-nuc-sprout-2_1_4.complete.cml.xml"));
		root=(CMLElement)doc.getRootElement();
		mol=(CMLMolecule) root.query("//cml:molecule", CMLConstants.CML_XPATH).get(0);
	}
	@Test
	public void testCalcDescriptor() {
		for (DescriptorTypes type : DescriptorTypes.values()) {
			Double value=type.calcDescriptor(mol,root);
			System.out.println(type.name()+": "+value);
			Assert.assertNotNull(value);
		}
	}
	

}
