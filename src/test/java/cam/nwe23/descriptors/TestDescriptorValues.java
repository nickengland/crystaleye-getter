package cam.nwe23.descriptors;

import static org.junit.Assert.*;

import java.io.File;
import java.io.IOException;
import java.util.Map;

import junit.framework.Assert;

import nu.xom.Document;
import nu.xom.ParsingException;
import nu.xom.ValidityException;

import org.junit.Before;
import org.junit.Test;
import org.xmlcml.cml.base.CMLBuilder;
import org.xmlcml.cml.base.CMLConstants;
import org.xmlcml.cml.element.CMLMolecule;
import org.xmlcml.cml.tools.SMILESTool;

public class TestDescriptorValues {

CMLMolecule mol;
	
	@Before
	public void setUp() throws ValidityException, ParsingException, IOException {
		CMLBuilder builder = new CMLBuilder();
		Document doc=builder.build(new File("src/test/resources/testCML/zs2105sup1_I_ring-nuc-sprout-2_1_4.complete.cml.xml"));
		mol=(CMLMolecule)doc.getRootElement().query("//cml:molecule", CMLConstants.CML_XPATH).get(0);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testDescriptorValues() {
		DescriptorValues values = new DescriptorValues(null);
	}

	@Test
	public void testNotNull() {
		DescriptorValues values = new DescriptorValues(mol);
		Assert.assertNotNull(values);
	}

	@Test
	public void testGetMap() {
		DescriptorValues values = new DescriptorValues(mol);
		Map<DescriptorTypes, Double> map=values.getMap();
		Assert.assertNotNull(map);
		Assert.assertEquals(DescriptorTypes.values().length, map.size());
	}

	@Test
	public void testGetMol() {
		DescriptorValues values = new DescriptorValues(mol);
		Assert.assertEquals(mol, values.getMol());
	}

}
