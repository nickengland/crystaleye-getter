package cam.nwe23.descriptors;

import java.io.File;
import java.io.IOException;

import junit.framework.Assert;

import nu.xom.Document;
import nu.xom.ParsingException;
import nu.xom.ValidityException;

import org.junit.Before;
import org.junit.Test;
import org.xmlcml.cml.base.CMLBuilder;
import org.xmlcml.cml.base.CMLConstants;
import org.xmlcml.cml.element.CMLMolecule;
import org.xmlcml.cml.tools.SMILESTool;


public class TestMinMaxValue {
CMLMolecule mol;
	
	@Before
	public void setUp() throws ValidityException, ParsingException, IOException {
		CMLBuilder builder = new CMLBuilder();
		Document doc=builder.build(new File("src/test/resources/testCML/zs2105sup1_I_ring-nuc-sprout-2_1_4.complete.cml.xml"));
		mol=(CMLMolecule)doc.getRootElement().query("//cml:molecule", CMLConstants.CML_XPATH).get(0);
	}
	
	@Test
	public void testMinMax(){
		DescriptorValues desc= new DescriptorValues(mol);
		MinMaxValue minMax = new MinMaxValue(2.0, desc);
		Assert.assertNotNull(minMax);
		Assert.assertEquals(2.0, minMax.getNumericValue(),0.0001);
		Assert.assertEquals(desc, minMax.getDescriptorValue());
	}
	
	
}
