package cam.nwe23.descriptors;

import static org.junit.Assert.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import nu.xom.Document;
import nu.xom.ParsingException;
import nu.xom.ValidityException;

import org.junit.Assert;
import org.junit.Test;
import org.xmlcml.cml.base.CMLBuilder;
import org.xmlcml.cml.base.CMLElement;

import cam.nwe23.config.ConfigurationLoader;

public class TestValueFilter {

	@Test
	public void testValueFilterStringStringString() {
		ValueFilter filter = new ValueFilter("iucr:cell_measurement_temperature", "200", ">");
		Assert.assertNotNull(filter);
		assertEquals("200", filter.value);
		assertEquals("//cml:property[@dictRef='iucr:cell_measurement_temperature']", filter.xpath);
	}

	@Test
	public void testValueFilterConfiguration() {
		ValueFilter filter = new ValueFilter(ConfigurationLoader.getStaticConfiguartion());
		assertNotNull(filter);
	}

	@Test
	public void testFilterByValue() throws ValidityException, ParsingException, IOException{
		ValueFilter filter = new ValueFilter("iucr:cell_measurement_temperature", "200", ">");
		CMLBuilder builder = new CMLBuilder();
		Document doc=builder.build(this.getClass().getResourceAsStream("/testCML/testFilter.cml"));
		List<Comparable> list=filter.getMatchingValues((CMLElement)doc.getRootElement());
		assertEquals(1, list.size());
	}
	

	@Test
	public void testAddCMLElement() throws ValidityException, ParsingException, IOException {
		ValueFilter filter = new ValueFilter("iucr:cell_measurement_temperature", "200", ">");
		CMLBuilder builder = new CMLBuilder();
		Document doc=builder.build(this.getClass().getResourceAsStream("/testCML/testFilter.cml"));
		CMLElement root = (CMLElement)doc.getRootElement();
		filter.addCMLElement(root);
		assertEquals("296.0", filter.outputMap.get(root));
		filter= new ValueFilter("iucr:cell_measurement_temperature", "296.0", ">=");
		filter.addCMLElement(root);
		assertEquals("296.0", filter.outputMap.get(root));
	}
	
	@Test
	public void testEqualsString() throws ValidityException, ParsingException, IOException{
		ValueFilter filter = new ValueFilter("iucr:symmetry_space_group_name_hall", "-P 2ybc", "=");
		CMLBuilder builder = new CMLBuilder();
		Document doc=builder.build(this.getClass().getResourceAsStream("/testCML/testFilter.cml"));
		CMLElement root = (CMLElement)doc.getRootElement();
		filter.addCMLElement(root);
		assertEquals("-P 2ybc", filter.outputMap.get(root));
	}
	@Test
	public void testAddAllCmlElement() throws ValidityException, ParsingException, IOException {
		ValueFilter filter = new ValueFilter("iucr:cell_measurement_temperature", "199", ">");
		CMLBuilder builder = new CMLBuilder();
		Document doc=builder.build(this.getClass().getResourceAsStream("/testCML/testFilter.cml"));
		CMLElement root = (CMLElement)doc.getRootElement();
		CMLElement root2= (CMLElement)builder.build(this.getClass().getResourceAsStream("/testCML/testFilter2.cml")).getRootElement();
		List<CMLElement> addAll = new ArrayList<CMLElement>();
		addAll.add(root);
		addAll.add(root2);
		filter.addAllCmlElement(addAll);
		assertEquals(2, filter.outputMap.size());
	}
}
