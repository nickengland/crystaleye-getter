package cam.nwe23.io;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import cam.nwe23.descriptors.DescriptorMinMax;
import cam.nwe23.descriptors.DescriptorTypes;

public class TestReadFiles {

	File dir;
	
	@Before
	public void setUp(){
	 dir = new File("src/test/resources/testCML");
	}
	
	@Test
	public void testReadFilesFile() {
		ReadFiles readFiles= new ReadFiles(dir);
		System.out.println(readFiles.files.size());
		Assert.assertTrue(readFiles.files.size()>1);
	}

	@Test
	public void testReadFilesListOfFile() {
		List<File> list= new ArrayList<File>();
		list.add(new File("src/test/resources/testCML/zs2105sup1_I_ring-nuc-sprout-2_1_4.complete.cml.xml"));
		list.add(new File("src/test/resources/testCML/zs2105sup1_I_ring-nuc_1_1.complete.cml.xml"));
		ReadFiles readFiles = new ReadFiles(list);
		Assert.assertEquals(2, readFiles.files.size());
	}

	@Test
	public void testCalcMinMaxDescriptors() {
		ReadFiles readFiles= new ReadFiles(dir);
		readFiles.calcMinMaxDescriptors();
		DescriptorMinMax results=readFiles.getMinMax();
		System.out.println(results.getMaxNumericValue(DescriptorTypes.NUMBEROFATOMS));
		Assert.assertTrue(results.getMaxNumericValue(DescriptorTypes.NUMBEROFATOMS)>22);
		System.out.println(results.getMinNumericValue(DescriptorTypes.NUMBEROFATOMS));
		Assert.assertTrue(results.getMinNumericValue(DescriptorTypes.NUMBEROFATOMS)<12);
	}

}
