package cam.nwe23.mongo;

import java.io.IOException;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.xmlcml.cml.base.CMLElement;

import wwmm.pubcrawler.model.Article;
import wwmm.pubcrawler.model.Issue;
import wwmm.pubcrawler.model.Journal;
import wwmm.pubcrawler.model.id.JournalId;
import wwmm.pubcrawler.types.Doi;

public class TestMongoFetcher {

	@Test
	public void testInit() throws InterruptedException, IOException {
		MongoFetcher fetch = new MongoFetcher();
		Assert.assertNotNull(fetch);
		Article article = fetch.getArticleFromDoi(new Doi("http://dx.doi.org/10.1021/ic050962c"));
		System.out.println(article.toString());
		List<CMLElement> list = fetch.getCifsFromArticleAsCML(article);
		
		for (CMLElement cml : list) {
			Assert.assertNotNull(cml);
		}
	}
	
	@Test
	public void testListJournals(){
		MongoFetcher fetch = new MongoFetcher();
		List<Journal> list=fetch.store.listJournals();
		System.out.println(list);
	}
	@Test
	public void testFindJournal(){
		MongoFetcher fetch = new MongoFetcher();
		Journal journal=fetch.store.findJournal(new JournalId("acta/e"));
		System.out.println(journal);
	}
	@Test
	public void testGetLatestActaE(){
		MongoFetcher fetch = new MongoFetcher();
		Issue issue=fetch.getLatestActaE();
		Assert.assertNotNull(issue);
		System.out.println(issue);
		String id = (String)issue.get("id");
		Assert.assertTrue(id.compareTo("acta/e/2011/06-00")>0);
	}
}
