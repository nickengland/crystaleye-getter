package cam.nwe23.spider;

import org.junit.Assert;
import org.junit.Test;


public class TestActaEResolver {

	@Test
	public void testRetrieveCached(){
		String doi="http://dx.doi.org/10.1107/S1600536807033521";
		ActaEResolver res = new ActaEResolver();
		CifBundle bundle=res.resolveDoi(doi);
		Assert.assertNotNull(bundle);
		Assert.assertNotNull(bundle.summary);
		Assert.assertEquals("download.zip/10.1107_S1600536807033521.html", bundle.summary.getPath());
	}
}
