package cam.nwe23.wordpress;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.UUID;

import javax.imageio.ImageIO;

import nu.xom.Document;
import nu.xom.ParsingException;
import nu.xom.ValidityException;

import org.apache.commons.io.FileUtils;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.xmlcml.cml.base.CMLBuilder;
import org.xmlcml.cml.element.CMLMolecule;
import org.xmlcml.cml.tools.SMILESTool;

import redstone.xmlrpc.XmlRpcException;
import redstone.xmlrpc.XmlRpcFault;
import redstone.xmlrpc.XmlRpcStruct;

public class TestImageUploader {

	private static File tmpDir;
	
	@BeforeClass
	public static void initTmpDir() throws IOException {
		File dir; 
		do {
			dir = new File("target/tmp-"+UUID.randomUUID());
		} while (dir.exists());
		FileUtils.forceMkdir(dir);
		tmpDir = dir;
	}

	@AfterClass
	public static void cleanup() {
		FileUtils.deleteQuietly(tmpDir);
	}
	
	private File getImageFile() {
		File file;
		do {
			file = new File(tmpDir, UUID.randomUUID()+".png");
		} while (file.exists());
		return file;
	}
	
	
	CMLMolecule testmol;
	@Before
	public void setUpMol() throws ValidityException, ParsingException, IOException{
		InputStream stream=TestImageUploader.class.getResourceAsStream("/testCML/zs2105sup1_I_ring-nuc_1_1.complete.cml.xml");
		CMLBuilder bob = new CMLBuilder();
		Document doc=bob.build(stream);
		testmol=(CMLMolecule)doc.getRootElement();
	}
	
	@Test
	public void testStoreMolAsTempFile() throws IOException {
		File temp=ImageUploader.storeMolAsTempFile(testmol);
		Assert.assertNotNull(temp);
		Assert.assertTrue(temp.exists());
		temp.deleteOnExit();
	}

	@Test
	public void testGetImageTempFile() throws IOException {
		File temp = ImageUploader.getImageTempFile();
		try {
			Assert.assertNotNull(temp);
			Assert.assertTrue(temp.exists());
		} finally {
			FileUtils.deleteQuietly(temp);
		}
	}

	@Test
	public void testCreateImage() throws IOException {
		File image=ImageUploader.createImage(ImageUploader.storeMolAsTempFile(testmol), getImageFile());
		Assert.assertNotNull(image);
		Assert.assertTrue(image.exists());
		BufferedImage img = ImageIO.read(image);
		Assert.assertEquals(-1, img.getRGB(299, 299));
		Assert.assertEquals(-1, img.getRGB(0, 299));
		Assert.assertEquals(-1, img.getRGB(299, 0));
	}

	@Test
	public void testFail() throws IOException{
		SMILESTool tool = new SMILESTool();
		tool.parseSMILES("CCO");
		File image = ImageUploader.createImage(ImageUploader.storeMolAsTempFile(tool.getMolecule()), getImageFile());
		Assert.assertNotNull(image);
		Assert.assertTrue(image.exists());
		BufferedImage img = ImageIO.read(image);
		Assert.assertEquals("ff000000", Integer.toHexString(img.getRGB(299, 299)));
		Assert.assertEquals("ff000000", Integer.toHexString(img.getRGB(0, 299)));
		Assert.assertEquals("ff000000", Integer.toHexString(img.getRGB(299, 0)));
	}
	
	@Test
	public void testUpload() throws IOException, XmlRpcException, XmlRpcFault{
		File testFile = new File("src/test/resources/image.png");
		ImageUploader uploader = new ImageUploader();
		XmlRpcStruct reply=uploader.uploadImage(testFile, "test.png");
		String url=(String)reply.get("url");
		Assert.assertTrue(url.endsWith(".png"));
	}
	
}
