package cam.nwe23.wordpress;

import java.io.IOException;
import java.io.InputStream;
import java.util.Map;

import junit.framework.Assert;
import nu.xom.Document;
import nu.xom.Element;
import nu.xom.ParsingException;
import nu.xom.ValidityException;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.xml.sax.SAXException;
import org.xmlcml.cml.base.CMLBuilder;
import org.xmlcml.cml.base.CMLConstants;
import org.xmlcml.cml.element.CMLMolecule;

import redstone.xmlrpc.XmlRpcException;
import redstone.xmlrpc.XmlRpcFault;

import cam.nwe23.config.ConfigurationLoader;
import cam.nwe23.io.Builder;
import de.schlichtherle.truezip.file.TFile;
import de.schlichtherle.truezip.file.TFileInputStream;


public class TestPostFormatter {
	CMLMolecule mol;
	Element blurb;
	@Before
	public void setUp() throws ValidityException, ParsingException, IOException, SAXException{

		
		//TFile.setDefaultArchiveDetector(new TDefaultArchiveDetector("zip"));
		
		CMLBuilder cmlBuilder = new CMLBuilder();
		Document doc=cmlBuilder.build(TestPostFormatter.class.getResourceAsStream("/testCML/zs2105sup1_I_ring-nuc_1_1.complete.cml.xml"));
		mol=(CMLMolecule)doc.getRootElement().query("//cml:molecule", CMLConstants.CML_XPATH).get(0);
		Builder builder = new Builder();
		TFile file = new TFile("download.zip/10.1107_S1600536807033521.html");
		InputStream stream = new TFileInputStream(file);
		doc=builder.build(stream);
		blurb=doc.getRootElement();
	}
	
	@Test (expected=IllegalArgumentException.class)
	public void testNullConstructor() throws IOException{
		PostFormatter formatter = new PostFormatter(null,null, null, null);
	}
	
	@Test
	public void testConstructor() throws IOException{
		PostFormatter formatter = new PostFormatter(ConfigurationLoader.getStaticConfiguartion(),mol,blurb, "Test molecule");
		Assert.assertNotNull(formatter);
	}
	@Test
	public void testParser() throws IOException{
		PostFormatter formatter = new PostFormatter(ConfigurationLoader.getStaticConfiguartion(),mol, blurb,"Test molecule");
		Map<String, String> results=formatter.parseBlurb();
		Assert.assertEquals(2, results.size());
		Assert.assertEquals("2,2,3,3'-Tetraphenyl-7,7'-biquinoxaline", results.get("title"));
	}
	
	@Test
	public void testStriptags() throws IOException{
		String a="sdfsdofjsdf<sfsdfsdf>sdfsdfsd";
		String b="p<asda>ass";
		String c="p<adasdasdasd66asd>ass.<re><b/>";
		PostFormatter formatter = new PostFormatter(ConfigurationLoader.getStaticConfiguartion(),mol, blurb, "Test");
		Assert.assertEquals("sdfsdofjsdfsdfsdfsd",formatter.stripTags(a));
		Assert.assertEquals("pass",formatter.stripTags(b));
		Assert.assertEquals("pass.",formatter.stripTags(c));
	}
	
	@Test
	public void testFormatter() throws IOException, XmlRpcException, XmlRpcFault{
		PostFormatter formatter=new PostFormatter(ConfigurationLoader.getStaticConfiguartion(),mol, blurb, "Test molecule");
		formatter.createPostToWordpress().post(true);
	}
}
