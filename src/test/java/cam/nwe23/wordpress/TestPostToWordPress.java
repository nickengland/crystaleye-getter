package cam.nwe23.wordpress;

import java.net.MalformedURLException;
import java.util.Properties;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;

import cam.nwe23.config.Configuration;

import redstone.xmlrpc.XmlRpcException;
import redstone.xmlrpc.XmlRpcFault;

public class TestPostToWordPress {

	@Test
	public void testConstructer() throws MalformedURLException {
		PostToWordpress press = new PostToWordpress();
		Assert.assertNotNull(press);
	}

	@Test(expected = MalformedURLException.class)
	public void testmalformed() throws MalformedURLException {
		Properties props = new Properties();
		props.put("wordpressXmlRpcURL", "sdfgsdfsdf////sdf");
		props.put("proxyPort","1");
		PostToWordpress press = new PostToWordpress(new Configuration(props));
	}
	
	@Ignore
	@Test
	public void testpost() throws XmlRpcException, XmlRpcFault, MalformedURLException{
		PostToWordpress press= new PostToWordpress();
		press.setTitle("Test");
		press.setContent("<h1>This is a test post</h1> <p>Does it work?</p>");
		press.addCategory("test");
		String reply=press.post(false);
		Assert.assertTrue(Integer.parseInt(reply)>0);
	}
}
